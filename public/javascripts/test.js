var indexApp = angular.module("indexApp",['angularFileUpload','ui.bootstrap','restangular','growlNotifications', 'ngAnimate']).filter('toString', function(){
  return function(dataArray){
    return dataArray.toString();
  };
});
indexApp.factory('getListAPI', ['Restangular', function (Restangular) {
    //prepend /api before making any request with restangular
    var rest = Restangular.withConfig(function(RestangularConfigurer) {
      RestangularConfigurer.setFullResponse(true);
    });
    rest.setBaseUrl('/');
    return {
      orders: {
        fetch: function (query) {
          return rest.all('get_orders').getList(query);
        }
      },
    };
}]);
indexApp.controller('appController',['$scope','$http','FileUploader','getListAPI',function($scope,$http, FileUploader,getListAPI){
	$scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.filter={};
    $scope.filter.company='none';
    $scope.filter.page = 1;
    $scope.filter.limit = $scope.itemsPerPage;
    $scope.filter.search = "";
    $scope.deleteOrder = function(){
        $scope.resetNotify();
        $http({method: 'post', url:'/delete_order', data:{orderid:$scope.deleteid}}).
            success(function(response){
              if(response.success){
                $scope.filter.search="";
                $scope.filter.page=1;
                $scope.selectOrderPage();
                $scope.successNotify="delete successfully";
              }
              else{
                $scope.errorNotify=response.errors[0];
              }
            }).
            error(function(response){
                $scope.errorNotify="sorry, connection error, please try it again";
            });
    }
    $scope.resetNotify = function(){
        $scope.successNotify= false;
        $scope.errorNotify=false;
    }
    $scope.searchOrder = function(){
        $scope.descendingorder=false;
        $scope.filter.page=1;
        $scope.selectOrderPage();
    }
    $scope.fetchOrderResult = function(){
        $scope.loading=true;
        return getListAPI.orders.fetch($scope.filter).then(function(result){
          $scope.loading=false;
          $scope.OrderList= result.data;
          $scope.totalOrderPages = result.headers('X-page-count');
          $scope.totalOrderItems = result.headers('X-item-count');
        }, function(){
          $scope.OrderList=[];
          $scope.totalOrderPages=0;
          $scope.totalOrderItems=0;
        })
    };
    $scope.selectOrderPage = function () {
        $scope.fetchOrderResult();
    };
    $scope.getCompany = function(){
        $http({method: 'get', url:'/all_company'}).
            success(function(response){
              if(response.success){
                    $scope.CompanyList = response.companies;
              }
              else{
              }
            }).
            error(function(response){

            });
    };
    $scope.getOrderCount = function(){
        $http({method: 'get', url:'/count_orders'}).
            success(function(response){
              if(response.success){
                $scope.OrderCountList = response.orderCount;
              }
              else{
              }
            }).
            error(function(response){

            });
    }
    $scope.getCompany();
    $scope.selectOrderPage();
    $scope.$watch('filter.company', function(newitem, olditem){
        if(newitem!=olditem){
          $scope.descendingorder=false;
          $scope.filter.search="";
          $scope.filter.page=1;
          $scope.selectOrderPage();
        }
    });
    $scope.$watch('descendingorder', function(newitem, olditem){
        if(newitem!=olditem){
          if (newitem == true) {
            $scope.getOrderCount();
          }
          else{
          }
        }
    })
    var uploader = $scope.uploader = new FileUploader({
	      url: '/upload/'
	});
	uploader.filters.push({
	    name: 'sizeFilter',
	    fn: function(item /*{File|FileLikeObject}*/,options){
	      if(item.size>100000){
	        alert('File is too large');
	        return false;
	      }
	      else
	        return true;
	    }
	});
	uploader.queueLimit = 1;
    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
    };
    uploader.onAfterAddingFile = function(fileItem) {
        $scope.uploadedFileOriName=fileItem['file'].name;
    };
    uploader.onRemoveItem = function(fileItem, response, status, headers) {
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        $scope.resetNotify();
        if(!response)
            return $scope.errorNotify="sorry, connection error, please try it again";
        if (response.success) {
            $scope.successNotify="upload successfully";
            fileItem.remove();
            $scope.filter.company='none'
            $scope.filter.search='';
            $scope.filter.page=1;
            $scope.getCompany();
            $scope.selectOrderPage();
        }
        else{
          $scope.errorNotify = response.errors[0];
        }
    };
}]);
