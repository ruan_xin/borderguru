var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');
var OrderModel = require('../models/order');
var lineReader = require('line-reader');
function publicFilePath(filename){
    return path.resolve('public', './docs/'+filename);
};
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Borderguru' });
});
router.get('/all_company/', function(req, res, next){
  var workflow = req.app.utility.workflow(req, res);
  workflow.on('getCompany', function(){
    OrderModel.distinct('companyName', function(err, items){
      if(err) return workflow.emit('exception',err);
      workflow.outcome.success = true;
      workflow.outcome.companies = items;
      workflow.emit('response');
    })
  })
  workflow.emit('getCompany');
});
router.post('/delete_order/', function(req, res, next){
  var workflow = req.app.utility.workflow(req, res);
  console.log(req.body);
  workflow.on('validate', function(){
    if(parseInt(req.body.orderid)>=1)
      workflow.emit('deleteOrder');
    else
      workflow.emit('exception','unvalid order id');
  });
  workflow.on('deleteOrder', function(){
    OrderModel.findOne({'orderId':req.body.orderid}, function(err, order){
      if (err) return workflow.emit('exception', err);
      if (!order) {
        console.log('no order');
        workflow.outcome.success=false;
        workflow.outcome.errors.push('no such order');
        return workflow.emit('response');
      }
      else{
        console.log('has order');
        order.remove(function(err){
          if (err) return workflow.emit('exception', err);
          workflow.outcome.success=true;
          workflow.emit('response');
        });
      }
    })
  });
  workflow.emit('validate');
});
router.get('/count_orders', function(req, res, next){
    var workflow  = req.app.utility.workflow(req, res);
  workflow.on('countOrders', function(){
      OrderModel.aggregate([{
        $group:{
          _id: "$orderedItem",
          orderIds:{$push: "$orderId"},
          companyNames:{$push: "$companyName"},
          customerAdresss:{$push: "$customerAdress"},
          count:{$sum:1}
        }
      }]).sort({'count':-1}).exec(function(err, items){
        if (err) return workflow.emit('exception', err);
        workflow.outcome.success=true;
        workflow.outcome.orderCount=items;
        workflow.emit('response');
      });
  })
  workflow.emit('countOrders');
});
router.get('/get_orders/', function(req, res, next){
  var workflow  = req.app.utility.workflow(req, res);
  workflow.on('getlist', function(){
    var searchstring = req.query.search.substring(0,50);
    var p_search={};
    var p_company={};
    if(searchstring)
      p_search={'customerAdress':RegExp(searchstring, 'i')};
    if(req.query.company!='none')
      p_company={'companyName':req.query.company}
    var p_container={$and:[p_search, p_company]};
    OrderModel.paginate(p_container, req.query.page, req.query.limit, function(err, pageCount, item_list, itemCount) {
        if (err)
          return workflow.emit('exception', err);
        res.format({
          json: function(){
            res.setHeader("X-page-count", pageCount);
            res.setHeader("X-item-count", itemCount);
            res.send(item_list);
          }
        })
    }, {sortBy : { 'orderId' : 1 }})
  });
  workflow.emit('getlist');
});
router.post('/upload/', function(req, res, next) {
  var workflow = req.app.utility.workflow(req, res);
  workflow.on('validate', function(){
		if(req.files['file'].size>100000)
            workflow.outcome.errors.push('data is too large');
    if (workflow.hasErrors()) {
		    return workflow.emit('response');
		}
		workflow.emit('dropCurrentOrders');
  });
  // workflow.on('writeFile', function(){
  //   fs.rename(req.files['file'].path, publicFilePath(req.files['file'].name), function(err) {
  //     workflow.emit('dropCurrentOrders');
  //   });
  // })
  workflow.on('dropCurrentOrders', function(){
    OrderModel.remove({}, function(err){
      if (err) return workflow.emit('exception',err);
      workflow.emit('readFile');
    })
  });
  workflow.on('readFile', function(){
      var data =[];
      lineReader.eachLine(req.files['file'].path, function(line, last) {
        var order_params_array = line.split(", ");
        data.push(order_params_array);
        if (last) {
          console.log(data);
          return workflow.emit('addEachOrder',data);
        }
      });
  })
  workflow.on('addEachOrder', function(data){
    require('async').each(data, function(order_params,callback){
      var newOrder = new OrderModel();
      newOrder.orderId = order_params[0];
      newOrder.companyName = order_params[1];
      newOrder.customerAdress = order_params[2];
      newOrder.orderedItem = order_params[3];
      newOrder.save(function(err){
        if (err) return callback(err);
        callback();
      })
    }, function(err){
      if (err) return workflow.emit('exception',err);
      workflow.outcome.success = true;
      workflow.emit('response');
    })
  });
  workflow.emit('validate');
});
module.exports = router;
