var path = require('path');
module.exports = function(grunt) {
	grunt.initConfig({
		copy:{
			vendor:{
				files:[
          		{
            		expand: true, cwd: 'bower_components/angular/',
            		src: ['angular.js'], dest: 'public/vendor/angular/'
          		},
          		{
		            expand: true, cwd: 'bower_components/angular-bootstrap/',
		            src: ['ui-bootstrap-tpls.js'], dest: 'public/vendor/angular-bootstrap/'
		        },
		        {
	            	expand: true, cwd: 'bower_components/angular-animate/',
	            	src: ['angular-animate.js'], dest: 'public/vendor/angular-animate/'
	            },
		        {
	            	expand: true, cwd: 'bower_components/restangular/',
	            	src: ['dist/restangular.js'], dest: 'public/vendor/restangular/'
	            },
	            {
	            	expand: true, cwd: 'bower_components/angular-growl-notifications/',
	            	src: ['dist/angular-growl-notifications.js'], dest: 'public/vendor/angular-growl-notifications/'
	            },
	            {
		        	expand: true, cwd: 'bower_components/es5-shim/',
		        	src: ['es5-shim.js','es5-sham.js'], dest: 'public/vendor/es5-shim/'
		        },
	            {
		            expand: true, cwd: 'bower_components/lodash/',
		            src: ['lodash.js'], dest: 'public/vendor/lodash/'
		        },
          		{
            		expand: true, cwd: 'bower_components/angular-file-upload/',
            		src: ['angular-file-upload.js'], dest: 'public/vendor/angular-file-upload/'
          		}]
			}
		},
		uglify:{
			layouts:{
				files:{
					'public/javascripts/core.min.js':[
					'public/vendor/es5-shim/es5-shim.js',
            		'public/vendor/es5-shim/es5-sham.js',
            		'public/vendor/lodash/lodash.js',
					'public/vendor/angular/angular.js',
					'public/vendor/angular-growl-notifications/dist/angular-growl-notifications.js',
					'public/vendor/angular-file-upload/angular-file-upload.js',
					'public/vendor/angular-bootstrap/ui-bootstrap-tpls.js',
					'public/vendor/angular-animate/angular-animate.js',
					'public/vendor/restangular/dist/restangular.js'
					]
				}
			}
		}
	});
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.registerTask('default', ['copy:vendor','uglify']);
}

