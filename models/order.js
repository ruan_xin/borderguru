var mongoose = require('mongoose');

var orderSchema = mongoose.Schema({
	orderId: {type:String, unique:true},
	companyName:{type:String, index:true},
	customerAdress:{type:String, index:true},
	orderedItem:{type:String},
});
orderSchema.plugin(require('mongoose-paginate'));

module.exports = mongoose.model('Order', orderSchema);